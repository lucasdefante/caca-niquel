package br.com.itau;

import java.util.Scanner;

public class IO {

    public static String inicio(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Olá, seja bem-vindo!\nEntre com seu nome para iniciar o jogo: ");
        return teclado.nextLine();
    }

    public static void imprimirSlot(Formas forma, int slot){
        System.out.println("Slot " + slot + ": " + forma.toString());
    }

    public static void premio(){
        System.out.println("Parabéns!!! Você ganhou a premiação!!!");
    }

    public static void encerrar(int valor){
        System.out.println("Pontuação final: " + valor);
    }

}
