package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Formas {

    private enum Simbolo {

        BANANA(10), FRAMBOESA(50), MOEDA(100), SETE(300);

        private int valor;

        Simbolo(int valor){
            this.valor = valor;
        }
    }

    private final Simbolo simbolo;
    private final int valor;

    private Formas(Simbolo simbolo){
        this.simbolo = simbolo;
        this.valor = simbolo.valor;
    }

    private static final List<Formas> formas = new ArrayList<Formas>();
    static {
        for (Simbolo simbolo: Simbolo.values()) {
            formas.add(new Formas(simbolo));
        }
    }

    public static ArrayList<Formas> getFormas() {
        return new ArrayList<Formas>(formas);
    }

    public int getValor() {
        return valor;
    }

    public Simbolo getSimbolo() {
        return simbolo;
    }

    public String toString(){
        return simbolo + " - " + valor;
    }
}
