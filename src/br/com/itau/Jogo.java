package br.com.itau;

import java.util.*;

public class Jogo {

    private Jogador jogador;
    private int slots;
    private int fatorPremiacao = 100;


    public Jogo (Jogador jogador){
        this.jogador = jogador;
    }

    public List<Formas> sortearFormas(int slots){
        Random rnd = new Random();
        List<Formas> sorteados = new ArrayList<Formas>();

        for(int i=0; i < slots; i++){
            sorteados.add(Formas.getFormas().get(rnd.nextInt(Formas.getFormas().size())));
        }
        return sorteados;
    }

    public void gerenciarJogada(int slots) throws Exception{
        this.slots = slots;
        List<Formas> sorteados = sortearFormas(slots);
        int posicao=1;
        int pontos=0;

        for (Formas forma : sorteados) {
            Thread.sleep(2000);
            IO.imprimirSlot(forma, posicao);
            posicao++;
            pontos += forma.getValor();
        }

        if(isPremiado(sorteados)){
            pontos = pontos*fatorPremiacao;
            IO.premio();
        }

        Thread.sleep(500);
        IO.encerrar(pontos);

    }

    public boolean isPremiado(List<Formas> sorteados){
        return new HashSet<Formas>(sorteados).size() == 1;
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }
}
